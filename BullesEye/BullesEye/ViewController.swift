//
//  ViewController.swift
//  BullesEye
//
//  Created by Gabriela Cuascota on 24/4/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var gameSlider: UISlider!
    
    /*var target = 0
    var score = 0
    var roundGame = 1*/
    
    let gameModel = Game()
    
    var cheat = 0
    
    @IBAction func aboutButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameModel.restartGame()
        setValues()
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int (gameSlider.value)
        gameModel.play(sliderValue: sliderValue)
        /*
        switch sliderValue{
        case target:
            score += 100
        case (target - 2)...(target + 2):
            score += 50
        case (target - 5)...(target + 5):
            score += 10
        default:
            break*/
        }
        
        /*roundGame += 1
        target = Int (arc4random_uniform(100))
        scoreLabel.text = "\(score)"
        targetLabel.text = "\(target)"
        roundLabel.text = "\(roundGame)"*/
    //}
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        gameModel.restartGame()
        setValues()
    }
    
    @IBAction func infoButtonPressed(_ sender: Any) {
    }

    @IBAction func WinnerButtonPressed(_ sender: Any) {
        if gameModel.isWinner {
            performSegue(withIdentifier: "toWinnerSegue", sender: self)
        }
    }
    
    /*private func restartGame(){
        target = Int (arc4random_uniform(100))
        score = 0
        roundGame = 1
        scoreLabel.text = "0"
        targetLabel.text = "\(arc4random_uniform(100))"
        roundLabel.text = "1"
    }*/
    
    func setValues (){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLabel.text = "\(gameModel.roundGame)"
        setValues()
    }
    

}


