//
//  NewItemViewController.swift
//  ToDo
//
//  Created by Gabriela Cuascota on 9/5/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class NewItemViewController: UIViewController {

    
    @IBOutlet weak var titleTextFile: UITextField!
    @IBOutlet weak var locationTextFile: UITextField!    
    @IBOutlet weak var descriptionTextFile: UITextField!
    
    var itemManager: ItemManagement?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        let itemTitle = titleTextFile.text ?? ""
        let itemLocation = locationTextFile.text ?? ""
        let itemDescription = descriptionTextFile.text ?? ""
        
        let item = Item(
            title: itemTitle,
            location: itemLocation,
            description: itemDescription
        )
        
        /*if item.title == " "{
            return print("el campo esta vacio")
        }*/

        
    if item.title == " " {
    showAlert(title: "ERROR", message: "Title is required")
        }
        itemManager?.toDoItems += [item]
    }

    func showAlert(title:String, message: String){
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let okAction = UIAlertAction (title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}


