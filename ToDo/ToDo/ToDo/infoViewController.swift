//
//  infoViewController.swift
//  ToDo
//
//  Created by Gabriela Cuascota on 16/5/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class infoViewController: UIViewController {


    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var desclabel: UILabel!
    
    var itemInfo:(itemManager: ItemManagement, index: Int)?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].title
        locationLabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].location
        desclabel.text = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!].description
    }

   
    
    

}
