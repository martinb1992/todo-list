//
//  ItemManager.swift
//  ToDo
//
//  Created by Gabriela Cuascota on 9/5/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import Foundation

class ItemManagement {

    var toDoItems:[Item] = []
    var doneItems:[Item] = []
    
    
    
    func checkItem(index:Int){
        let item = toDoItems.remove(at: index)
        doneItems += [item]
    }
    
    
    func unCheckItem(index:Int){
        let item = doneItems.remove(at: index)
        toDoItems += [item]
    }
    
}
