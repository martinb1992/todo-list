//
//  Item.swift
//  ToDo
//
//  Created by Gabriela Cuascota on 9/5/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import Foundation

struct Item{
    let title: String
    let location: String
    let description: String
}
